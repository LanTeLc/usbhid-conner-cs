﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NUnit.Framework;
using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using HID;

namespace HidConnerCs
{
    public partial class Form1 : Form
    {
        private Hid usbHid;
        bool IsConnect = false;
        byte var = 0;
        bool IsKey = false;
        bool IsKeyC = false;

        public Form1()
        {
            InitializeComponent();
            usbHid = new Hid();
            usbHid.DeviceArrived += new EventHandler(usbHid_DeviceArrived);
            usbHid.DataReceived += new EventHandler<report>(usbHid_DataReceived);
        }

        void usbHid_DataReceived(object sender, EventArgs e)
        {
            report myRP = (report)e;
            if (InvokeRequired)
            {
                Invoke(new EventHandler(usbHid_DataReceived), new object[] { sender, e });
            }
            else
            {
                //textBox1.Text = "接收到数据";
                tbInfo.Text += (myRP.reportID).ToString() + ":" + Hid.ByteToHexString(myRP.reportBuff) + "\r\n";
                if (myRP.reportID == 7)
                {
                    progressBar1.Value = myRP.reportBuff[0];
                    label3.Text = "ADC返回值:" + myRP.reportBuff[0].ToString();
                }
                if(myRP.reportID == 5)
                {
                    if (myRP.reportBuff[0]==0) btKey.BackColor = Color.Gray;
                    else btKey.BackColor = Color.Red;
                }
            }
        }

        void usbHid_DeviceArrived(object sender, EventArgs e)
        {
            report myRP = (report)e;
            if (InvokeRequired)
            {
                Invoke(new EventHandler(usbHid_DeviceArrived), new object[] { sender, e });
            }
            else
            {
                tbInfo.Text = "设备连接";
            }
        }
        protected override void WndProc(ref Message m)
        {
            try
            {
                usbHid.ParseMessages(ref m);

                base.WndProc(ref m); // pass message on to base form
            }
            catch (Exception e)
            {

            }
        }


        /// <summary>
        /// 字符串转16进制字节数组
        /// </summary>
        /// <param name="hexString"></param>
        /// <returns></returns>
        private static UInt16 strToToHexByte(string hexString)
        {
            hexString = hexString.Replace(" ", "");
            if ((hexString.Length % 2) != 0)
                hexString += " ";
            byte[] returnBytes = new byte[hexString.Length / 2];
            for (int i = 0; i < returnBytes.Length; i++)
                returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
            return (UInt16)((UInt16)returnBytes[0] * (UInt16)256 + (UInt16)returnBytes[1]);
        }
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
               // usbI = new USBInterface(textBox1.Text, textBox2.Text);
               // IsConnect = usbI.Connect();
                usbHid.PID = strToToHexByte(tbPID.Text);
                usbHid.VID = strToToHexByte(tbVid.Text);
                if (usbHid.CheckDevicePresent())
                {
                    IsConnect = true;
                    MessageBox.Show("连接ok");
                    //usbI.startRead();
                    //usbI.enableUsbBufferEvent(myEventCacher);
                    toolStripStatusLabel2.Text = "设备连接成功！";
                    //timer1.Enabled = true;
                }
                else
                {
                    toolStripStatusLabel2.Text = "设备连接不成功，请检查硬件！";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        bool led1 = false;
        bool led2 = false;
        bool led3 = false;
        bool led4 = false;
        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                byte[] transferData = new byte[this.usbHid.OutputReportLength - 1];
                if (IsConnect == false)
                {
                    MessageBox.Show("请先连接设备！");
                    return;
                }
                transferData[0] = 1;
                if (led1)
                {
                    led1 = false;
                    transferData[0] = 0;
                    btLED1.BackColor = Color.Red;
                }
                else
                {
                    led1 = true;
                    btLED1.BackColor = Color.Gray;
                }
                //transferData[0] = 0x01;

                report myRP = new report(1, transferData);

                usbHid.Write(myRP);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void btLED2_Click(object sender, EventArgs e)
        {
            try
            {
                byte[] transferData = new byte[this.usbHid.OutputReportLength - 1];
                if (IsConnect == false)
                {
                    MessageBox.Show("请先连接设备！");
                    return;
                }
                transferData[0] = 1;
                if (led2)
                {
                    led2 = false;
                    transferData[0] = 0;
                    btLED2.BackColor = Color.Red;
                }
                else
                {
                    led2 = true;
                    btLED2.BackColor = Color.Gray;
                }
                //transferData[0] = 0x01;

                report myRP = new report(2, transferData);

                usbHid.Write(myRP);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btLED3_Click(object sender, EventArgs e)
        {
            try
            {
                byte[] transferData = new byte[this.usbHid.OutputReportLength - 1];
                if (IsConnect == false)
                {
                    MessageBox.Show("请先连接设备！");
                    return;
                }
                transferData[0] = 1;
                if (led3)
                {
                    led3 = false;
                    transferData[0] = 0;
                    btLED3.BackColor = Color.Red;
                }
                else
                {
                    led3 = true;
                    btLED3.BackColor = Color.Gray;
                }
                //transferData[0] = 0x01;

                report myRP = new report(3, transferData);

                usbHid.Write(myRP);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btLED4_Click(object sender, EventArgs e)
        {
            try
            {
                byte[] transferData = new byte[this.usbHid.OutputReportLength - 1];
                if (IsConnect == false)
                {
                    MessageBox.Show("请先连接设备！");
                    return;
                }
                transferData[0] = 1;
                if (led4)
                {
                    led4 = false;
                    transferData[0] = 0;
                    btLED4.BackColor = Color.Red;
                }
                else
                {
                    led4 = true;
                    btLED4.BackColor = Color.Gray;
                }
                //transferData[0] = 0x01;

                report myRP = new report(4, transferData);

                usbHid.Write(myRP);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            try
            {
                usbHid.CloseDevice();
                toolStripStatusLabel2.Text = "设备已断开连接！";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                toolStripStatusLabel2.Text = "设备已断开连接！";
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }


        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            usbHid.CloseDevice();
        }

        private void btClear_Click(object sender, EventArgs e)
        {
            tbInfo.Text = "";
        }

        private void btList_Click(object sender, EventArgs e)
        {
            tbInfo.Text += usbHid.ListHidDevice(0,0);
        }
    }
}
